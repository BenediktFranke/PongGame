from setuptools import setup

setup(
    name="NeuralPong",
    version="0.1",
    description="A Pong Game and a neural network learning to play it!",
    author="Benedikt Franke",
    author_email="benefranke@t-online.de",
    url="https://gitlab.com/BenediktFranke/PongGame",
    install_requires=["numpy", "tensorflow", "kivy"]
)